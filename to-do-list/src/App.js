import React, { useState } from "react";
import { Grid, TextField, Button } from "@material-ui/core";
import './App.css';

function App() {
const [toDoList, setToDoList] = useState([{ toDo: "" }]);

    const handleInputChange = (e, index) => {
        const { name, value } = e.target;
        const list = [...toDoList];
        list[index][name] = value;
        setToDoList(list);
    };

    const handleRemoveClick = index => {
        const list = [...toDoList];
        list.splice(index, 1);
        setToDoList(list);
    };

    const handleAddClick = () => {
        setToDoList([...toDoList, { toDo: "" }]);
    };

    return (
        <Grid container style={{ padding: "2rem" }}>
            <Grid className="center" item xs={12} sm={12} md={12}>
                <h1>MY TODO LIST</h1>
            </Grid>
            {toDoList.map((x, i) => {
                return (
                    <React.Fragment key={i}>
                        <Grid item style={{marginTop: "1rem"}} xs={12} sm={12} md={10}>
                            <TextField
                                name="toDo"
                                id="outlined-search"
                                label="Tâche"
                                type="search"
                                variant="outlined"
                                fullWidth={true}
                                placeholder="Tâche à faire"
                                value={x.toDo}
                                onChange={e => handleInputChange(e, i)}
                            />
                        </Grid>
                        <Grid style={{textAlign: "center", marginTop: "1rem"}} item xs={12} sm={12} md={2}>
                            <Button
                                className="end-button"
                                onClick={() => handleRemoveClick(i)}>
                                END TASK
                            </Button>
                        </Grid>
                    </React.Fragment>
                );
            })}
            <Grid item style={{ textAlign: "center", padding: "2rem"}} xs={12} sm={12} md={12}>
                <Button
                    className="add-button"
                    onClick={() => handleAddClick()}
                >
                    ADD TASK
                </Button>
            </Grid>
        </Grid>
    );
}

export default App;
